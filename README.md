# Download datasets
Script for downloading datasets. Allows automation of data adquisition and can be easily integrated on CI/CD pipeline.

## Execution example
```bash
python downloadRepo.py -REPO $DATASET_WGET -DOWNLOAD_TO path
```
